#/bin/bash

:'
multi-line comments
'
git pull
git status
echo $TEMP_DIR
if [[ $TEMP_DIR = *"temp" ]] ; then
    rm -Rf $TEMP_DIR/*
fi
cd $TEMP_DIR
pwd
echo enter zipUrl to download:
read zipUrl
wget -v --user=$BITBUCKET_USER --password=$BITBUCKET_PASSWORD $zipUrl
jar xvf *.zip

#if [ ! -f ".*.zip" ]; then
#    echo "no files downloaded, exiting"
#    exit;
#fi


echo EMORYOIT_HOME=$EMORYOIT_HOME

arr=( "$EMORYOIT_HOME/deployment/esb/dev" "$EMORYOIT_HOME/deployment/esb/qa" "$EMORYOIT_HOME/deployment/esb/staging" )

## now loop through the above array
for i in "${arr[@]}"
do
   echo updating this folder: $i
    cd  $i
    pwd
    svn update
    svn status
    echo "continue?[y/n]"
    read userInput
    if [ $userInput = "n" ]; then
        exit;
    fi
    echo procesing $i
    rm -Rf  message/releases/com/amazon/aws
    rm -Rf  EnterpriseObjects/3.0/com/amazon/aws
    cp -R $TEMP_DIR/message/releases/com/amazon/* message/releases/com/amazon
    cp -R $TEMP_DIR/moagen/eos/com/amazon/* EnterpriseObjects/3.0/com/amazon

    sdeleting message/releases/com/amazon/aws
    sadding  message/releases/com/amazon/aws
    scommit  message/releases/com/amazon/aws

    sadding EnterpriseObjects/3.0/com/amazon/aws
    sdeleting  EnterpriseObjects/3.0/com/amazon/aws
    scommit  EnterpriseObjects/3.0/com/amazon/aws
done

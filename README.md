# RHEDcloud MOAs #

## What is this repository for? ##

The repository stores the [message definitions](./message/releases/org/rhedcloud) for the `org.rhedcloud` domain.


### Pipeline ###
The default pipeline runs the `generateAndBuildAllMoas` command from the [servicegen app]() and produces the OpenEAI message API. This consists of both the Java/JMS client library and the XML-based Enterprise Objects (EOs) parts of the API.

The build artifacts are stored in the [Downloads](https://bitbucket.org/rhedcloud/rhedcloud-moas/downloads/) section of the repo.



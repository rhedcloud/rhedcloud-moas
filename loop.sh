#!/bin/sh

START=`pwd`
cd message/releases
PACS=
for i in $(find . -type d -print);
do
  if [ "`basename $i`" = "Resources" ]; then
    L=`echo $i | tr "/" " " `
    PAC="null"
    for e in $L
    do
      if [ "$e" != "Resources" ] && [ "$e" != "." ]; then
        if [ "$PAC" = "null" ]; then
          PAC=$e
        else
          PAC=$PAC.$e
        fi
      fi
    done
    echo moa.package.prefix=$PAC
    PACS="$PACS $PAC"
  fi
done
for pac in $PACS
do
  if [ "$pac" = "org.openeai" ]; then
    echo Skipping openeai
  else
    echo Processing $pac
    BASE=`echo $pac | tr "." "/"`
    CATS=`ls $BASE`
    for cat in $CATS
    do
      if [ "$cat" != "Resources" ]; then
        echo Generating WSDL for category named $cat
        echo moa.package.prefix=$pac > $START/service.properties
        echo project.prefix=rhedcloud >> $START/service.properties
        echo service.name=${cat}Service >> $START/service.properties
        echo service.password=CredentialRedactedAndRotated >> $START/service.properties
        echo service.name.prefix=org.rhedcloud >> $START/service.properties
        echo wsdl.tns=http://www.rhedcloud.org/ >> $START/service.properties
        echo segment.dtd.filepath=$BASE/Resources/1.0/Segments.dtd >> $START/service.properties
        echo message.category.filepath=$BASE/$cat >> $START/service.properties
        echo wsdl.address.dev=http://emory-srd-service-dev.ws.serviceforge.net/services >> $START/service.properties
        echo wsdl.address.qa=http://emory-srd-service-test.ws.serviceforge.net/services >> $START/service.properties
        echo wsdl.address.stage=http://emory-srd-service-stage.ws.serviceforge.net/services >> $START/service.properties
        echo wsdl.address.prod=http://emory-srd-service-prod.ws.serviceforge.net/services >> $START/service.properties
      fi
    done
    cat $START/service.properties
  fi
done
cd $START

#!/bin/sh

echo
echo "OpenEAI Example Enterprise Execution System"
echo "-------------------------------------------"
echo

if [ "$JAVA_HOME" = "" ] ; then
  echo "ERROR: JAVA_HOME not found in your environment."
  echo
  echo "Please, set the JAVA_HOME variable in your environment to match the"
  echo "location of the Java Virtual Machine you want to use.\n"
  exit 1
fi

if [ "$OPENEAI_HOME" = "" ] ; then
  echo "ERROR: OPENEAI_HOME not found in your environment."
  echo
  echo "Please, set the OPENEAI_HOME variable in your environment to match the"
  echo "location of the OpenEAI Examples distribution you've un-packed.\n"
  exit 1
fi

if [ "$1" = "" ] ; then
  echo "ERROR: Invalid number of arguments."
  echo
  echo "Please provide a 'target' that you wish to start."
  echo "Targets are specified in the build.xml file."
  exit 1
fi

if [ `echo $OSTYPE | grep -n cygwin` ]; then
  PS=";"
else
  PS=":"
fi
OPENEAI_HOME=.
OPENEAI_RUNTIME=$OPENEAI_HOME/configs/messaging/Environments/Examples/ export OPENEAI_RUNTIME
OPENEAI_LIB=$OPENEAI_HOME/lib
BUILD_FILE=$OPENEAI_HOME/build.xml export BUILD_FILE
LOCALCLASSPATH=$JAVA_HOME/lib/tools.jar${PS}$OPENEAI_LIB/ant.jar
ANT_HOME=$OPENEAI_LIB

echo Starting $1
echo

echo Starting Ant...
echo

# One person found a seg fault with jdk 1.3.0 on Linux where adding -classic
# to the following line fixed the issue

$JAVA_HOME/bin/java -Dant.home=$ANT_HOME -classpath $LOCALCLASSPATH org.apache.tools.ant.Main -buildfile $BUILD_FILE $*
